import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/services/auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  public user:any = {};

  public formSignUp:FormGroup;

  public error:any = {
    show:false
  };

  constructor(public authService:AuthService) { 
    
    this.formSignUp = new FormGroup({
      user: new FormControl('',[Validators.required,Validators.email]),
      password: new FormControl('',[Validators.required,Validators.minLength(6)])
    });
  }

  ngOnInit() {
  }

  public signUp(){

    this.authService.SignUp(this.formSignUp.value).then(response => {
      this.authService.SetUserData(response.user);
    }).catch(errors => {
      this.error.show = true;
      this.error.message = errors.message;
    });
  }

}
