import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';

import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public user:any = {};

  public formLogin:FormGroup;

  public error:any = {
    show:false
  };

  constructor(public authService:AuthService,public _router:Router) { 
    
    this.authService.SignOut();
    
    this.formLogin = new FormGroup({
      user: new FormControl('',[Validators.required,Validators.email]),
      password: new FormControl('',Validators.required)
    });
  }

  ngOnInit() {
  }

  public toLogin(){
    console.log(this.formLogin.value);
    
    this.authService.SignIn(this.formLogin.value).then(response => {
      console.log(response);
      this.authService.SetUserData(response.user);
      this._router.navigate(['/dashboard']);
    }).catch(errors => {
      this.error.show = true;
      this.error.message = errors.message;
    });
  }

}
