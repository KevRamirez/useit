import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';

import { FormControl, FormGroup, Validators } from '@angular/forms';

import { Router } from '@angular/router';


@Component({
  selector: 'app-recover-password',
  templateUrl: './recover-password.component.html',
  styleUrls: ['./recover-password.component.css']
})
export class RecoverPasswordComponent implements OnInit {

  public user:any = {};

  public formLogin:FormGroup;

  public error:any = {
    show:false
  };

  constructor(public authService:AuthService,public router:Router) { 

    this.formLogin = new FormGroup({
      user: new FormControl('',[Validators.required,Validators.email])
    });
  }

  ngOnInit() {
  }

  public sendEmail(){
    console.log(this.formLogin.value);
    
    this.authService.ForgotPassword(this.formLogin.value.user).then(response => {
      window.alert('Correo enviado correctamente, revisa tu buzón.');
      this.router.navigate(['/']);
    }).catch(errors => {
      this.error.show = true;
      this.error.message = errors.message;
    });
  }

}
