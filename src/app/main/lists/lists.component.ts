import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray } from '@angular/forms';
import { FirestoreService } from 'src/services/firestore.service';
import { Observable } from 'rxjs';
import { AuthService } from 'src/services/auth.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-lists',
  templateUrl: './lists.component.html',
  styleUrls: ['./lists.component.css']
})
export class ListsComponent implements OnInit {

  public publishForm: FormGroup;
  public publishFormModal: FormGroup;
  public userForm: FormGroup;
  public commentsChild: any = [];
  public comments: any = [];
  public posts: any = [];
  public userData: any = {};
  public formChieldComment: FormArray = new FormArray([]);
  public formComment: FormArray = new FormArray([]);

  public limitPost = 2;
  public throttle = 300;
  public scrollDistance = 1;

  public fileData: any;

  public modalPostUpdate: any;

  constructor(public _fireStorage: FirestoreService, public _authService: AuthService, private modalService: NgbModal) {
    this.userData = this._authService.getDataUser();
    this.publishForm = new FormGroup({
      comment: new FormControl(''),
      img: new FormControl('')
    });
    this.publishFormModal = new FormGroup({
      comment: new FormControl(''),
      img: new FormControl('')
    });
    this.userForm = new FormGroup({
      displayName: new FormControl(''),
      photoURL: new FormControl(''),
    });
  }

  ngOnInit() {
    this.getChildrens();
    this.getComments();
    this.getPosts();
  }

  onScrollDown() {
    console.log('scrolled!!');
    this.limitPost += 2;
  }

  onUp(ev) {
    console.log('scrolled up!', ev);

  }

  public fileProgress(fileInput) {
    this.fileData = <File>fileInput.target.files[0];
    var reader = new FileReader();
    reader.readAsDataURL(this.fileData);
    reader.onload = (_event) => {
      this.publishForm.controls.img.setValue(reader.result);
    }
  }

  public fileProgressModal(fileInput) {
    this.fileData = <File>fileInput.target.files[0];
    var reader = new FileReader();
    reader.readAsDataURL(this.fileData);
    reader.onload = (_event) => {
      this.publishFormModal.controls.img.setValue(reader.result);
    }
  }

  public filePerfil(fileInput) {
    let fileData = <File>fileInput.target.files[0];
    var reader = new FileReader();
    reader.readAsDataURL(fileData);
    reader.onload = (_event) => {
      this.userForm.controls.photoURL.setValue(reader.result);
    }
  }

  public getChildrens() {
    this._fireStorage.getChildrens().subscribe(response => {
      this.commentsChild = [];
      response.forEach((childrenData: any) => {
        this.commentsChild.push({
          id: childrenData.payload.doc.id,
          data: childrenData.payload.doc.data()
        });
      });
    });
  }

  public getComments() {
    this._fireStorage.getComments().subscribe(response => {
      this.comments = [];
      response.forEach((commentData: any) => {
        let childs = this.getChildForComment(commentData.payload.doc.data().childs);
        let data = {
          id: commentData.payload.doc.id,
          data: commentData.payload.doc.data(),
          limitChildren: 2
        };
        data.data.childsDescription = childs;

        this.comments.push(data);
        let formGroup: FormGroup = new FormGroup({
          comment: new FormControl('')
        });
        this.formChieldComment.push(formGroup);
      });
    });
  }

  public getPosts() {
    this._fireStorage.getPosts().subscribe(response => {
      this.posts = [];
      response.forEach((postData: any) => {
        let comments = this.getCommentsForPost(postData.payload.doc.data().comments);
        let data = {
          id: postData.payload.doc.id,
          data: postData.payload.doc.data(),
          limitComment: 2
        }
        data.data.commentsDescription = comments;
        this.posts.push(data);

        let formGroupPost: FormGroup = new FormGroup({
          comment: new FormControl('')
        });
        this.formComment.push(formGroupPost);
      })
    });
    console.log(this.posts);
  }

  public getCommentsForPost(comments) {
    let arrReturnComments = [];
    for (let i in comments) {
      let comment = this.comments.filter(element => { return element.id == i });
      if (comment.length) {
        arrReturnComments.push(comment[0]);
      }
    }

    return arrReturnComments;
  }

  public getChildForComment(childs) {
    let arrReturnChilds = [];
    for (let i in childs) {
      let child = this.commentsChild.filter(element => { return element.id == i });
      if (child.length) {
        arrReturnChilds.push(child[0]);
      }
    }

    return arrReturnChilds;
  }

  public sharedPosts() {
    console.log(this.publishForm.value);

    let data = this.publishForm.value;

    data.user = this.userData;

    this._fireStorage.savePosts(data);
  }

  public addComment(post, groupForm) {
    console.log(post, groupForm);
    let data = groupForm;

    data.user = this.userData;

    this._fireStorage.saveComment(data).then(response => {
      console.log(response.id);
      this._fireStorage.pushComments(post, response.id);
    });
  }

  public addChildComment(comment, groupForm) {
    console.log(comment, groupForm);

    let data = groupForm;

    data.user = this.userData;

    this._fireStorage.saveChildComment(data).then(response => {
      console.log(response.id);
      this._fireStorage.pushChildrenComment(comment, response.id);
    });
  }

  public deletePost(post) {
    this._fireStorage.deletePost(post.id);
  }

  public showModalPost(post, content) {
    console.log(post);

    this.publishFormModal.reset();
    this.modalService.open(content, {}).result.then((result) => {
      let data = this.publishFormModal.value;
      if (!data.img) {
        delete (data.img);
      }
      if (!data.comment) {
        delete (data.comment);
      }
      this._fireStorage.updatePost(post.id, this.publishFormModal.value);
    }, (reason) => {
      console.log("hola");
    });
  }

  public showInfoPerfil(content){

    this.userForm.controls.displayName.setValue(this.userData.displayName);
    this.modalService.open(content, {}).result.then((result) => {
      let data = this.userForm.value;
      if(!data.img){
        delete(data.img);
      }
      this._authService.updateUser(data);
    }, (reason) => {
      console.log("hola");
    });
  }


}
