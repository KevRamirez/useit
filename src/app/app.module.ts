import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { LoginComponent } from './main/login/login.component';
import { ListsComponent } from './main/lists/lists.component';
import { RecoverPasswordComponent } from './main/recover-password/recover-password.component';
import { SignupComponent } from './main/signup/signup.component';

import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from "@angular/fire/auth";
import { AngularFirestoreModule } from '@angular/fire/firestore';

import { ReactiveFormsModule } from '@angular/forms';

import { FIREBASECONFIG } from '../config/const';

import { AuthGuard } from '../services/auth-guard.service';

import { InfiniteScrollModule } from 'ngx-infinite-scroll';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

const appRoutes: Routes = [
  { 
    path: 'login', 
    component: LoginComponent 
  },
  {
    path: 'recover-password',
    component: RecoverPasswordComponent
  },
  {
    path: 'signup',
    component: SignupComponent
  },
  {
    path: 'dashboard',
    component: ListsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  { path: '**', redirectTo: '/login' }
];

@NgModule({
  declarations: [
    LoginComponent,
    ListsComponent,
    AppComponent,
    RecoverPasswordComponent,
    SignupComponent
  ],
  imports: [
    AngularFireModule.initializeApp(FIREBASECONFIG),
    AngularFireAuthModule,
    AngularFirestoreModule,
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    ReactiveFormsModule,
    InfiniteScrollModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
