import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FirestoreService {
  constructor(
    private firestore: AngularFirestore
  ) {}
  
  public savePosts(data){
    return this.firestore.collection('posts').add(data);
  }

  public saveComment(data) {
    return this.firestore.collection('comments').add(data);
  }

  public saveChildComment(data){
    return this.firestore.collection('commentsChields').add(data);
  }

  public getChildrens(){
    return this.firestore.collection('commentsChields').snapshotChanges();
  }

  public getComments(){
    return this.firestore.collection('comments').snapshotChanges();
  }

  public getPosts(){
    return this.firestore.collection('posts').snapshotChanges();
  }

  public pushComments(post,commentId){
    console.log(post);
 
    let comments = post.data.comments ? post.data.comments : {};
    comments[commentId] = true;

    let data = {
      comments:comments
    };
  
    this.firestore.collection('posts').doc(post.id).update(data)

  }

  public pushChildrenComment (comment,childId){
    console.log(comment);

    let childs = comment.data.childs ? comment.data.childs : {};
    childs[childId] = true;

    let data = {
      childs:childs
    };
    console.log(data);
    this.firestore.collection('comments').doc(comment.id).update(data)
  }

  public updatePost (postId,value){
    return this.firestore.collection('posts').doc(postId).update(value);
  }

  public deletePost (postId){
    return this.firestore.collection('posts').doc(postId).delete();
  }

  public updateUser (userId,data){
    return this.firestore.collection('users').doc(userId).update(data);
  }
  
}