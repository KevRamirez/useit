import { Injectable, NgZone } from '@angular/core';
import { auth } from 'firebase/app';
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Router } from "@angular/router";

@Injectable({
    providedIn: 'root'
})

export class AuthService {

    userData: any; // Save logged in user data

    constructor(
        public afs: AngularFirestore,   // Inject Firestore service
        public afAuth: AngularFireAuth, // Inject Firebase auth service
        public router: Router
    ) {

        
        this.afAuth.authState.subscribe(user => {
            if (user) {
                this.userData = user;
                localStorage.setItem('user', JSON.stringify(this.userData));
                JSON.parse(localStorage.getItem('user'));
            } else {
                localStorage.setItem('user', null);
                JSON.parse(localStorage.getItem('user'));
            }
        })
    }

    getDataUser() {
        let user = JSON.parse(localStorage.getItem('user'));

        return {
            uid: user.uid,
            email: user.email,
            displayName: user.displayName,
            photoURL: user.photoURL,
        }
    }

    SignIn(data) {
        return this.afAuth.auth.signInWithEmailAndPassword(data.user, data.password);
    }

    SignUp(data) {
        return this.afAuth.auth.createUserWithEmailAndPassword(data.user, data.password)
    }

    ForgotPassword(passwordResetEmail) {
        return this.afAuth.auth.sendPasswordResetEmail(passwordResetEmail)
    }

    get isLoggedIn(): boolean {
        const user = JSON.parse(localStorage.getItem('user'));
        return (user && user !== null) ? true : false;
    }

    SetUserData(user) {
        const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
        const userData: any = {
            uid: user.uid,
            email: user.email,
            displayName: user.displayName,
            photoURL: user.photoURL,
            emailVerified: user.emailVerified
        }
        return userRef.set(userData, {
            merge: true
        })
    }

    SignOut() {
        return this.afAuth.auth.signOut().then(() => {
            this.removeUserData();
            this.router.navigate(['/']);
        })
    }

    public removeUserData() {
        localStorage.removeItem('user');
    }

    public updateUser(data){
       let user = this.afAuth.auth.currentUser;

       return user.updateProfile(data);
    }

}